﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace CountryBlog.Models
{
    public class Country
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(2)]
        public string Code { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Independence date")]
        public DateTime IndepDate { get; set; }

        [MaxLength(200)]
        [Display(Name = "National Flag")]
        public string FlagSrc { get; set; }

        [MaxLength(200)]
        public string WikiLink { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }

        public List<Article> Articles { get; set; }
    }
}
