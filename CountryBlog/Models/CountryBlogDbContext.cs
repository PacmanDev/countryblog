﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CountryBlog.Models
{
    public class CountryBlogDbContext: IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        
        public CountryBlogDbContext(DbContextOptions<CountryBlogDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Country constraints
            builder.Entity<Country>()
                .HasMany(a => a.Articles);
           
        }
    }
}
