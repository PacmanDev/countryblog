﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CountryBlog.ViewModels;

namespace CountryBlog.ViewModels
{
    public class CountryArticleGroup
    {
        public string Name { get; set; }

        public List<CountryArticleItem> Items { get; set; }
    }
}
