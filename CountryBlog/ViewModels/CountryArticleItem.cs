﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryBlog.ViewModels
{
    public class CountryArticleItem
    {
        public string Article { get; set; }

        public string Code { get; set; }

        public string Date { get; set; }

        public string FlagSrc { get; set; }

        public string WikiLink { get; set; }

        public string CountryName { get; set; }
    }
}
