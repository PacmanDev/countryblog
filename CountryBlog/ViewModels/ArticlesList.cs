﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Web.Pagination;
using CountryBlog.Models;

namespace CountryBlog.ViewModels
{
    public class ArticlesList
    {
        public ArticlesList()
        {
            Paging = new PaginationSettings();
        }

        public string Query { get; set; } = string.Empty;

        public List<Article> Articles { get; set; } = null;

        public PaginationSettings Paging { get; set; }
    }
}