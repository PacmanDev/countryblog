﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CountryBlog.Models;
using CountryBlog.ViewModels;
using CountryBlog.Utils;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CountryBlog.Controllers
{
    [Authorize]
    public class CountryController : Controller
    {
        private const int DefaultPageSize = 10;
        private const int DefaultMaxPageItems = 5;

        private CountryBlogDbContext _db;

        public CountryController(CountryBlogDbContext context)
        {
            _db = context;
        }
        // GET: /<controller>/
        public async Task<IActionResult> CountryList()
        {
            ViewBag.Title = "Countries";
            var countries = await _db.Countries.ToListAsync();

            return View(countries);
        }

        public async Task<IActionResult> Articles(int pageNumber = 1)
        {
            int offset = (DefaultPageSize * pageNumber) - DefaultPageSize;

            var query = _db.Articles.Include(c => c.Country).OrderBy(x => x.Country.Name)
                .Select(p => p)
                .Skip(offset)
                .Take(DefaultPageSize);

            var result = new ArticlesList();
            result.Articles = await query.AsNoTracking().ToListAsync();
            result.Paging.TotalItems = await _db.Articles.CountAsync();
            result.Paging.CurrentPage = pageNumber;
            result.Paging.ItemsPerPage = DefaultPageSize;
            result.Paging.MaxPagerItems = DefaultMaxPageItems;

            return View(result);
        }

        public IActionResult SearchQuery(string query)
        {
            HttpContext.Session.Set("query", query);

            return new JsonResult(new { msg = "Cookie was written successfully!" });
        }

        public IActionResult Search(int? pageNumber)
        {
            var currentPageNum = pageNumber.HasValue ? pageNumber.Value : 1;
            var offset = (DefaultPageSize * currentPageNum) - DefaultPageSize;
            string query = HttpContext.Session.Get<string>("query");

            var filtered = _db.Articles.Include(c => c.Country).Where(p =>
                p.Text.Contains(query)
            )
            .OrderByDescending(p => p.Country.Name)
            .ToList();

            var model = new ArticlesList();

            model.Articles = filtered
            .Skip(offset)
            .Take(DefaultPageSize)
            .ToList();

            model.Paging.CurrentPage = currentPageNum;
            model.Paging.ItemsPerPage = DefaultPageSize;
            model.Paging.TotalItems = filtered.Count;
            model.Paging.MaxPagerItems = DefaultMaxPageItems;
            model.Query = query;

            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("Search", model);
            }
            return View(model);
        }

        public IActionResult DownloadResults()
        {
            string filename = "search.txt";
            string query = HttpContext.Session.Get<string>("query");
            string route = "/country/articledetails";
            string linkPtn = HttpContext.Request.Host + route + "?id=";
            StringBuilder builder = new StringBuilder();

            var filtered = _db.Articles.Select(i => new { i.Id, i.Text }).Where(p =>
                p.Text.Contains(query)
            )
            .OrderBy(p => p.Id)
            .ToList();
            
            string intro = string.Format("Search results for query: \"{0}\"\r\n{1}", query, DateTime.Now);
            builder.AppendLine(intro);

            foreach (var item in filtered)
            {
                builder.AppendLine(linkPtn + item.Id.ToString());
            }

            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           "wwwroot", filename);

            string filteredString = builder.ToString();
            
            return File(Encoding.UTF8.GetBytes(filteredString), "text/plain", Path.GetFileName(path));
        }

        public async Task<IActionResult> DownloadArticle(int id)
        {
            DateTime now = DateTime.Now;
            
            string filename = "searchArticle.txt";
            string query = HttpContext.Session.Get<string>("query");
            string route = "/country/articledetails";
            string linkPtn = HttpContext.Request.Host + route + "?id=";
            StringBuilder builder = new StringBuilder();

            var item = await _db.Articles.Select(a =>new { a.Name, a.Id, a.Text }).Where(i => i.Id == id).SingleAsync();

            string intro = string.Format("Search results for query: \"{0}\"\r\n{1}", query, DateTime.Now);
            builder.AppendLine(intro);

            builder.AppendLine(linkPtn + item.Id.ToString());

            var pattern = new Regex("<.*?>");
            var pureArticleText = pattern.Replace(item.Text, string.Empty);
            builder.AppendLine(pureArticleText);
            
            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           "wwwroot", filename);

            DateTime after = DateTime.Now;
            TimeSpan delta = after - now;
            builder.AppendLine(string.Format("Execution Time = {0}ms", delta.TotalMilliseconds));
            string finalString = builder.ToString();
            return File(Encoding.UTF8.GetBytes(finalString), "text/plain", Path.GetFileName(path));
        }

        public IActionResult ArticleDetails(int id)
        {
            var article = _db.Articles.Include(c => c.Country).Where(d => d.Id == id).First();

            return View(article);
        }

        public async Task<IActionResult> GetMagicNumber()
        {
            DateTime now = DateTime.Now;

            var articles = await _db.Articles.Select(t => t.Text).ToListAsync();
            long count = 0;
            
            foreach (var item in articles)
            {
                var matches = Regex.Matches(item, @"\d+");
                count += matches.Sum(i => long.Parse(i.Value));
            }
            DateTime after = DateTime.Now;
            TimeSpan delta = after - now;

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(string.Format("Magic number = {0}", count));
            builder.AppendLine(string.Format("Execution time = {0}", delta.TotalMilliseconds));

            var result = new JsonResult(new { result = builder.ToString() });

            return result;
        }

    }
}
