﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CountryBlog.ViewModels;
using CountryBlog.Models;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CountryBlog.Controllers
{
    [Authorize(Policy = "RequireAdmin")]
    public class AdminController : Controller
    {
        private CountryBlogDbContext _db;

        public AdminController(CountryBlogDbContext db)
        {
            _db = db;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult InitDb()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> InitDb([FromBody] CountryArticleGroup data)
        {
            var jsondata = data;
            var obj = new
            {
                msg = "Database init was successful!",
                arr = new List<CountryArticleItem>()
            };
            
            if (jsondata != null)
            {
                string wikiLinkPattern = "https://en.wikipedia.org";

                foreach (var item in jsondata.Items)
                {
                    var country = new Country();
                    var article = new Article();
                    //Country fields
                    if (item.Code == null)
                        obj.arr.Add(new CountryArticleItem {
                            CountryName = item.CountryName,
                            Code = item.Code,
                            Article = item.Article,
                            WikiLink = item.WikiLink
                        });
                    country.Name = item.CountryName;
                    country.Code = item.Code;
                    country.FlagSrc = item.FlagSrc;
                    country.WikiLink = wikiLinkPattern + item.WikiLink;
                    country.IndepDate = DateTime.Parse(item.Date);

                    //Article fields
                    article.Author = "WikiCountributors";
                    article.Country = country;
                    article.Date = DateTime.UtcNow;
                    article.Name = "MainPage";
                    article.Text = item.Article;

                    await _db.Countries.AddAsync(country);
                    await _db.Articles.AddAsync(article);

                }
                await _db.SaveChangesAsync();
            }
            var response = new JsonResult(obj);
            return response;
        }

        public async Task<JsonResult> ClearDb()
        {
            var countries = _db.Countries;
            var articles = _db.Articles;
            _db.Countries.RemoveRange(countries);
            _db.Articles.RemoveRange(articles);

            await _db.SaveChangesAsync();

            var response = new JsonResult(new
            {
                msg = "Cleared successful!"
            });
            return response;
        }
    }
}
