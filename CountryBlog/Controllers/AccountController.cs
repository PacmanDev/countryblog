﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CountryBlog.ViewModels;
using CountryBlog.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.Extensions.Options;

namespace CountryBlog.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<ApplicationUser> _manager;
        private SignInManager<ApplicationUser> _signInManager;
        private RoleManager<IdentityRole> _roleManager;
        
        IHostingEnvironment _appHostEnv;

        public AccountController(UserManager<ApplicationUser> manager, 
                                 SignInManager<ApplicationUser> signInManager,
                                 RoleManager<IdentityRole> roleManager,
                                 IHostingEnvironment hostingEnvironment)
        {
            _manager = manager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _appHostEnv = hostingEnvironment;
        }

        [AllowAnonymous]
        public IActionResult Signup()
        {
            ViewBag.Title = "Sign Up";
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Signup(SignUpViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _manager.CreateAsync(user, model.Password);

                // for Admin User code
                //var roleExist = await _roleManager.RoleExistsAsync("Admin");
                //await _manager.GetSecurityStampAsync(user);
                //if (!roleExist)
                //{
                //    await _roleManager.CreateAsync(new IdentityRole("Admin"));

                //}
                //await _manager.AddToRoleAsync(user, "Admin");
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: model.IsPersistant);
                    return RedirectToAction(nameof(CountryController.CountryList), "Country");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }

            }
            return View(model);
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            //ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Title = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email,
                                                                         model.Password,
                                                                         model.RememberMe,
                                                                         lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    if (Url.IsLocalUrl(returnUrl))
                        return RedirectToAction(returnUrl);
                    else
                        return RedirectToAction("CountryList", "Country");
                }
                else if (result.IsLockedOut)
                    return View("Lockout");
                else
                    ModelState.AddModelError("", "incorrect login or password");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("CountryList", "Country");
        }

        public IActionResult AccesDenied()
        {
            return Forbid();
        }
    }
}